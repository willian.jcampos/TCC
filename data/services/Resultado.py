#!/usr/bin/env python
# -*- coding: utf-8 -*-


class Resultado (object):
    """Classe responsável por realizar os cálculos de desempenho do detector"""

    @staticmethod
    def calcular(valores):
        """Realiza os cálculos
            :param valores: vetor contendo os valores[totalDetectado, pos, fp, fn]
            :type valores: list(str)

            :returns
                acerto: (double) valor decimal de acerto
                erro: (double) valor decimal do erro
                aproveitamento: (double) valor decimal do aproveitamento
        """

        total_deteccao = 0
        total_pos = 0
        total_falso_pos = 0
        total_falso_neg = 0

        total_objetos = 0

        for i in range(len(valores)):
            total_deteccao = total_deteccao + valores[i][0]
            total_pos = total_pos + valores[i][1]
            total_falso_pos = total_falso_pos + valores[i][2]
            total_falso_neg = total_falso_neg + valores[i][3]

            total_objetos = total_objetos + valores[i][1] + valores[i][3]

        acerto = (int((100 * total_pos) / total_deteccao)*100)/100
        if acerto == 0.0:
            erro = (int(100.0 - acerto)*100)/100
        else:
            erro = (int((100 * (total_falso_pos + total_falso_neg)) / total_deteccao)*100)/100

        return acerto, erro
