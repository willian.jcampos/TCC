#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import cv2
from data.services.Listar import Listar
from data.util.Codigos import verifica_codigo
from data.util.Locais import Locais


class Imagem(object):
    """Classe responsável pelos serviços relacionados à manipulação de imagens no sistema"""

    ##########################################################################################
    # Redimensionar
    ##########################################################################################
    @staticmethod
    def redimensionar(nome_projeto, tipo_imagem, tamanho):
        """Redimensiona a imagem de forma proporcional

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param tipo_imagem: define o tipo de imagem, sendo:
                p: positivas
                n: negativas
                t: teste
            :type tipo_imagem: str

            :param tamanho: tamanho máximo que as imagem assumirão
            :type tamanho: int
        """

        global cinza
        tipo_imagem = verifica_codigo(tipo_imagem)

        if tipo_imagem == "positivas":
            pasta_originais = Locais().pasta_imagens_positivas_originais(nome_projeto)
            cinza = Locais().pasta_imagens_positivas_editadas_cinza(nome_projeto)

        elif tipo_imagem == "negativas":
            pasta_originais = Locais().pasta_imagens_negativas_originais(nome_projeto)
            cinza = Locais().pasta_imagens_negativas_editadas_cinza(nome_projeto)
            tamanho *= 2

        elif tipo_imagem == "teste":
            pasta_originais = Locais().pasta_teste_imagens_originais(nome_projeto)

        else:
            print(("[ERROR] Tipo de Imagem '" + str(tipo_imagem) + "' indefinido. Processo finalizado."))
            print("         Classe: services.Imagem.py")
            print("         Método: redimensionar")
            return

        lista_arquivos = Listar().listar_imagens(pasta_originais)

        for i in range(len(lista_arquivos)):
            imagem_cinza = cv2.imread(lista_arquivos[i], cv2.IMREAD_GRAYSCALE)
            altura, largura = imagem_cinza.shape[:2]

            if largura > altura:
                percentual = float(tamanho) / float(largura)
                tamanho_desejado = int((altura * percentual))
                imagem_redimensionada = cv2.resize(imagem_cinza, (tamanho, tamanho_desejado), cv2.INTER_LINEAR)

            else:
                percentual = float(tamanho) / float(altura)
                tamanho_desejado = int((largura * percentual))
                imagem_redimensionada = cv2.resize(imagem_cinza, (tamanho_desejado, tamanho), cv2.INTER_LINEAR)

            nome_imagem = str(lista_arquivos[i])
            salvar = os.path.join(cinza, nome_imagem.split("\\")[-1])
            cv2.imwrite(salvar, imagem_redimensionada)

    ##########################################################################################
    # Aplicar Filtro
    ##########################################################################################
    def aplicar_filtro(self, nome_projeto, tipo_imagem, tipo_filtro):
        """Percorre a lista <tipo_filtro> e chama o filtro correspondente ao codigo

            :param str nome_projeto: nome do projeto ao qual os filtros serão aplicados nas imagens
            :type nome_projeto: str

            :param list() tipo_filtro: lista de valores correspondente a cada um dos filtros
                1: Cinza
                2: Equaliza Histograma
                3: CLAHE
            :type tipo_filtro: list

            :param tipo_imagem: tipo de imagem selecionada
                p: positiva
                n: negativa
            :type tipo_imagem: str
        """

        tipo_imagem = verifica_codigo(tipo_imagem)

        # Localiza a Pasta CINZA de acordo com o tipo de imagem
        if tipo_imagem == "positivas":
            local = Locais().pasta_imagens_positivas_editadas_cinza(nome_projeto)

        elif tipo_imagem == "negativas":
            local = Locais().pasta_imagens_negativas_editadas_cinza(nome_projeto)

        else:
            print(("[ERROR] Tipo de Imagem '" + str(tipo_imagem) + "' indefinido. Processo finalizado."))
            print("         Classe: services.Imagem.py")
            print("         Método: redimensionar")
            return

        lista_arquivos = Listar().listar_imagens(local)

        for i in range(len(tipo_filtro)):
            # Não há necessidade de se aplicar o filtro cinza, visto que o mesmo é executado automaticamente
            if tipo_filtro[i] == 1:
                pass

            # Aplica o filtro de Equalização do Histograma nas imagens
            elif tipo_filtro[i] == 2:
                self._equaliza(nome_projeto, lista_arquivos, tipo_imagem)

            # Aplica o filtro de CLAHE nas imagens
            elif tipo_filtro[i] == 3:
                self._clahe(nome_projeto, lista_arquivos, tipo_imagem)

            else:
                print("[ERROR] Tipo de Filtro '" + str(tipo_filtro) + "' inexistente. Processo finalizado.")
                print("        Classe: services.Imagem.py")
                print("        Método: redimensionar")
                return

    ##########################################################################################
    # Contar Imagens
    ##########################################################################################
    @staticmethod
    def contar_imagens(nome_projeto, tipo_imagem, categoria):
        """Conta e retorna a quantidade de imagens de um tipo em um projeto

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param tipo_imagem: define o tipo de imagem
                p: positivas
                n: negativas
                t: teste
            :type tipo_imagem: str

            :param categoria: define a categoria pertencente à imagem
                editadas
                editadas/cinza
                editadas/clahe
                editadas/histograma
                originais
                detectadas
            :type categoria: str
        """

        tipo_imagem = verifica_codigo(tipo_imagem)

        if tipo_imagem == "positivas" or tipo_imagem == "negativas":
            local = os.path.join(Locais().pasta_projeto(nome_projeto), tipo_imagem, categoria)

        elif tipo_imagem == "teste":
            local = os.path.join(Locais().pasta_projeto(nome_projeto), "teste", "imagens", categoria)

        else:
            print(("[ERROR] Tipo de Imagem '" + str(tipo_imagem) + "' indefinido. Processo finalizado."))
            print("         Classe: services.Imagem.py")
            print("         Método: contar_imagens")
            return

        quantidade = len(Listar().listar_imagens(local))

        return quantidade

    ##########################################################################################
    # Tamanho da imagem
    ##########################################################################################
    @staticmethod
    def tamanho_imagem(imagem):
        """Recebe o local de uma imagem e retorna seu tamanho

            :param imagem: local onde se encontra a imagem
            :type imagem: str

            :return list(int(altura), int(largura))
                altura: altura em pixels da imagem
                largura: largura em pixels da imagem
        """

        try:
            img = cv2.imread(imagem, 0)

        except IOError:
            print("[ERROR] Arquivo ou diretório inválido")
            print("        Classe: services.Imagem.py")
            print("        Método: tamanho_imagem")
            return

        altura, largura = img.shape[:2]

        return altura, largura

    ##########################################################################################
    # Filtros
    ##########################################################################################

    @staticmethod
    def _equaliza(nome_projeto, lista_arquivos, tipo_imagem):
        """Percorre uma lista de arquivos (path) e aplica a EQUALIZAÇÃO DO HISTOGRAMA nas configurações padrões

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param lista_arquivos: lista de arquivos contendo o path absoluto para cada uma das imagens CINZA+
            :type lista_arquivos: list

            :param tipo_imagem: define o tipo de imagem
                p: positivas
                n: negativas
            :type tipo_imagem: str
        """

        for i in range(len(lista_arquivos)):
            imagem = cv2.equalizeHist(cv2.imread(lista_arquivos[i], 0))

            nome_imagem = str(lista_arquivos[i])

            # Localiza a Pasta HISTOGRAMA do projeto de acordo com o tipo de imagem
            if tipo_imagem == "positivas":
                local = Locais().pasta_imagens_positivas_editadas_histograma(nome_projeto)

            elif tipo_imagem == "negativas":
                local = Locais().pasta_imagens_negativas_editadas_histograma(nome_projeto)

            else:
                print(("[ERROR] Tipo de Imagem '" + str(tipo_imagem) + "' indefinido. Processo finalizado."))
                print("         Classe: services.Imagem.py")
                print("         Método: _equaliza")
                return

            salvar = os.path.join(local, nome_imagem.split("\\")[-1])
            cv2.imwrite(salvar, imagem)

    @staticmethod
    def _clahe(nome_projeto, lista_arquivos, tipo_imagem):
        """Percorre uma lista de arquivos (path) e aplica a equalização por CLAHE nas configurações padrões

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param lista_arquivos: lista de arquivos contendo o path absoluto para cada uma das imagens
            :type lista_arquivos: list

            :param tipo_imagem: define o tipo de imagem
                p: positivas
                n: negativas
            :type tipo_imagem: str
        """

        for i in range(len(lista_arquivos)):
            clahe = cv2.createCLAHE()
            imagem = clahe.apply(cv2.imread(lista_arquivos[i], 0))

            nome_imagem = str(lista_arquivos[i])

            # Localiza a Pasta CLAHE do projeto de acordo com o tipo de imagem
            if tipo_imagem == "positivas":
                local = Locais().pasta_imagens_positivas_editadas_clahe(nome_projeto)

            elif tipo_imagem == "negativas":
                local = Locais().pasta_imagens_negativas_editadas_clahe(nome_projeto)

            else:
                print(("[ERROR] Tipo de Imagem '" + str(tipo_imagem) + "' indefinido. Processo finalizado."))
                print("         Classe: services.Imagem.py")
                print("         Método: _clahe")
                return

            salvar = os.path.join(local, nome_imagem.split("\\")[-1])
            cv2.imwrite(salvar, imagem)
