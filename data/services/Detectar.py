#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import cv2
from data.util.Locais import Locais


class Detectar(object):
    """Classe responsável por realizar as detecções em imagens e vídeos"""

    ##########################################################################################
    # Detectar Imagem
    ##########################################################################################
    @staticmethod
    def detectar_imagem(nome_projeto, img, arquivo_cascade, altura: int = 300, largura: int = 500):
        """Realiza a detecção do objeto em imagens

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param img: local onde a imagem está armazenada
            :type img: str(path)

            :param arquivo_cascade: linha referente ao arquivo cascade selecionado
            :type arquivo_cascade: str(path)

            :param altura: altura maxima da imagem salva (default 300)
            :type altura: int

            :param largura: tamanho maximo para largura da imagem salva (default 500)
            :type largura: int

            :return int: quantidade de objetos detectados
        """

        fonte = cv2.FONT_HERSHEY_COMPLEX_SMALL

        local_xml = os.path.join(Locais().pasta_cascade(nome_projeto), arquivo_cascade, "cascade.xml")

        classificador = cv2.CascadeClassifier(local_xml)

        imagem = cv2.imread(img)
        altura_real, largura_real = imagem.shape[: 2]

        if altura > altura_real or largura > largura_real:
            res = cv2.resize(imagem, (largura, altura), cv2.INTER_AREA)
        else:
            res = cv2.resize(imagem, (largura, altura), cv2.INTER_LINEAR)

        imagem_cinza = cv2.cvtColor(res, cv2.COLOR_BGR2GRAY)

        detecta_objeto = classificador.detectMultiScale(imagem_cinza, scaleFactor=1.1, minNeighbors=5, minSize=(100, 100), maxSize=(altura, largura))

        objetos_detectados = 0

        for (x, y, w, h) in detecta_objeto:
            cv2.rectangle(res, (x, y), (x + w, y + h), (255, 0, 0), 2)
            cv2.rectangle(res, (x - 1, y + 1), (x + w + 1, y - 13), (255, 0, 0), cv2.FILLED)
            cv2.putText(res, nome_projeto, (x + 1, y - 2), fonte, 0.6, (255, 255, 255))
            objetos_detectados += 1

        cv2.putText(res, arquivo_cascade, (5, res.shape[0] - 5), fonte, 0.5, (0, 0, 0))
        cv2.imwrite(os.path.join(Locais().pasta_teste_imagens_detectadas(nome_projeto), img.split("\\")[-1]), res)

        return objetos_detectados

    ##########################################################################################
    # Detectar Video
    ##########################################################################################
    @staticmethod
    def detectar_video(nome_projeto, video, arquivo_cascade):
        """Realiza a detecção do objeto em vídeos

            :param nome_projeto: define o nome do projeto
            :type nome_projeto: str

            :param video: local onde o vídeo está armazenada
            :type video: str(path)

            :param arquivo_cascade: linha referente ao arquivo cascade selecionado
            :type arquivo_cascade: str(path)

            :return str: nome do video utilizaod
        """

        local_xml = os.path.join(Locais().pasta_cascade(nome_projeto), arquivo_cascade, "cascade.xml")
        nome_video = video.split("\\")[-1]

        cap = cv2.VideoCapture(video)

        video_w = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        video_h = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

        salvar = os.path.join(Locais().pasta_teste(nome_projeto), "videos", nome_video)
        fourcc = cv2.VideoWriter_fourcc(*'DIVX')
        out = cv2.VideoWriter(salvar, fourcc, 20.0, (video_w, video_h))

        classificador = cv2.CascadeClassifier(local_xml)

        while cap.isOpened():
            ret, frame = cap.read()

            if ret:
                gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                detect = classificador.detectMultiScale(gray, scaleFactor=1.1, minNeighbors=35, minSize=(50, 50), maxSize=(600, 600))

                for (x, y, w, h) in detect:
                    cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                    print("Objeto detectado nas coordenadas: (%i, %i), (%i, %i)" % (x, y, (x + y), (y + h)))

                out.write(frame)

                if cv2.waitKey(30) & 0xFF == ord("q"):
                    break
            else:
                break

        cap.release()
        out.release()
        cv2.destroyAllWindows()

        print("Processo finalizado!")

        return nome_video
