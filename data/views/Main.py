#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import wx
from data.services.Imagem import Imagem
from data.services.Listar import Listar
from data.services.Projeto import Projeto
from data.views.ApagarProjeto import ApagarProjeto
from data.views.NovoProjeto import NovoProjeto
from data.views.Testar import Testar
from data.views.TreinarTipoImagem import TreinarTipoImagem
from data.views.DetalhesXML import DetalhesXML


class Main(wx.Frame):

    def __init__(self):

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Sistema de Detecção de Objetos", pos=wx.DefaultPosition, size=wx.Size(600, 463), style=wx.CAPTION | wx.CLOSE_BOX | wx.MINIMIZE_BOX | wx.TAB_TRAVERSAL)

        icon = wx.Icon(os.path.join(os.path.dirname(__file__), "..", "..", "icons", "icon.ico"), wx.BITMAP_TYPE_ANY)
        self.SetIcon(icon)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer_frame = wx.BoxSizer(wx.VERTICAL)

        self.panelMain = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer_painel = wx.BoxSizer(wx.HORIZONTAL)

        sb_sizer_projetos = wx.StaticBoxSizer(wx.StaticBox(self.panelMain, wx.ID_ANY, "Projetos"), wx.VERTICAL)

        self.panel4 = wx.Panel(sb_sizer_projetos.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(150, -1), wx.TAB_TRAVERSAL)
        b_sizer133 = wx.BoxSizer(wx.VERTICAL)

        self.buttonNovoProjeto = wx.Button(self.panel4, wx.ID_ANY, "Novo Projeto", wx.DefaultPosition, wx.Size(100, 30), 0)
        self.buttonNovoProjeto.SetToolTip("Novo Projeto")
        b_sizer133.Add(self.buttonNovoProjeto, 0, wx.EXPAND, 0)

        self.staticline3 = wx.StaticLine(self.panel4, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        b_sizer133.Add(self.staticline3, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        list_box_lista_projetos_choices = []
        self.listBoxListaProjetos = wx.ListBox(self.panel4, wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, 270), list_box_lista_projetos_choices, 0)
        b_sizer133.Add(self.listBoxListaProjetos, 1, wx.EXPAND, 0)

        self.panel4.SetSizer(b_sizer133)
        self.panel4.Layout()
        sb_sizer_projetos.Add(self.panel4, 1, wx.EXPAND, 0)

        b_sizer_painel.Add(sb_sizer_projetos, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer18 = wx.StaticBoxSizer(wx.StaticBox(self.panelMain, wx.ID_ANY, "Informações do Projeto"), wx.VERTICAL)

        self.panel2 = wx.Panel(sb_sizer18.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(420, 400), wx.TAB_TRAVERSAL)
        b_sizer44 = wx.BoxSizer(wx.VERTICAL)

        b_sizer_info_projeto = wx.BoxSizer(wx.VERTICAL)

        sb_sizer_detalhes_projeto = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, "Detalhes do Projeto"), wx.HORIZONTAL)

        b_sizer48 = wx.BoxSizer(wx.VERTICAL)

        self.staticText6 = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "Nome do Projeto:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText6.Wrap(-1)
        b_sizer48.Add(self.staticText6, 0, wx.ALL | wx.EXPAND, 5)

        self.staticText9 = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "Imagens Positivas:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText9.Wrap(-1)
        b_sizer48.Add(self.staticText9, 0, wx.ALL | wx.EXPAND, 5)

        self.staticText91 = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "Imagens Negativas:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText91.Wrap(-1)
        b_sizer48.Add(self.staticText91, 0, wx.ALL, 5)

        self.staticText911 = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "Imagens de Teste:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText911.Wrap(-1)
        b_sizer48.Add(self.staticText911, 0, wx.ALL, 5)

        sb_sizer_detalhes_projeto.Add(b_sizer48, 0, wx.ALL, 0)

        b_sizer49 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextNomeProjeto = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer49.Add(self.staticTextNomeProjeto, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        self.staticTextNumPos = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNumPos.Wrap(-1)
        self.staticTextNumPos.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer49.Add(self.staticTextNumPos, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        self.staticTextNumNeg = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNumNeg.Wrap(-1)
        self.staticTextNumNeg.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer49.Add(self.staticTextNumNeg, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        self.staticTextNumImgTeste = wx.StaticText(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNumImgTeste.Wrap(-1)
        self.staticTextNumImgTeste.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer49.Add(self.staticTextNumImgTeste, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)

        sb_sizer_detalhes_projeto.Add(b_sizer49, 1, wx.ALL, 0)

        b_sizer19 = wx.BoxSizer(wx.VERTICAL)

        self.buttonApagarProjeto = wx.Button(sb_sizer_detalhes_projeto.GetStaticBox(), wx.ID_ANY, "Apagar", wx.DefaultPosition, wx.Size(130, 30), 0)
        self.buttonApagarProjeto.SetToolTip("Apagar Projeto")

        b_sizer19.Add(self.buttonApagarProjeto, 0, wx.ALIGN_RIGHT | wx.TOP, 75)

        sb_sizer_detalhes_projeto.Add(b_sizer19, 1, wx.EXPAND, 5)

        b_sizer_info_projeto.Add(sb_sizer_detalhes_projeto, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.RIGHT, 5)

        sb_sizer_treinamentos_realizados = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, "Treinamentos Realizados"), wx.HORIZONTAL)

        b_sizer16 = wx.BoxSizer(wx.HORIZONTAL)

        list_box_treinamentos_choices = []
        self.listBoxTreinamentos = wx.ListBox(sb_sizer_treinamentos_realizados.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(-1, 170), list_box_treinamentos_choices, 0)
        b_sizer16.Add(self.listBoxTreinamentos, 1, 0, 0)

        sb_sizer_treinamentos_realizados.Add(b_sizer16, 1, wx.ALL | wx.EXPAND, 5)

        b_sizer13 = wx.BoxSizer(wx.VERTICAL)

        b_sizer14 = wx.BoxSizer(wx.VERTICAL)

        self.bpButtonView = wx.BitmapButton(sb_sizer_treinamentos_realizados.GetStaticBox(), wx.ID_ANY, wx.Bitmap(os.path.join(os.path.dirname(__file__), "..", "..", "icons", "View.png"), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.bpButtonView.SetToolTip("Detalhes")
        self.bpButtonView.Enable(False)

        b_sizer14.Add(self.bpButtonView, 0, wx.EXPAND, 0)

        self.bpButtonApplication = wx.BitmapButton(sb_sizer_treinamentos_realizados.GetStaticBox(), wx.ID_ANY, wx.Bitmap(os.path.join(os.path.dirname(__file__), "..", "..", "icons", "Play.png"), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        self.bpButtonApplication.SetToolTip("Iniciar Teste")
        b_sizer14.Add(self.bpButtonApplication, 0, wx.EXPAND, 0)

        b_sizer13.Add(b_sizer14, 1, 0, 0)

        b_sizer15 = wx.BoxSizer(wx.VERTICAL)

        self.bpButtonDelete = wx.BitmapButton(sb_sizer_treinamentos_realizados.GetStaticBox(), wx.ID_ANY, wx.Bitmap(os.path.join(os.path.dirname(__file__), "..", "..", "icons", "Delete.png"), wx.BITMAP_TYPE_ANY), wx.DefaultPosition, wx.DefaultSize, wx.BU_AUTODRAW)
        b_sizer15.Add(self.bpButtonDelete, 0, wx.ALIGN_BOTTOM | wx.EXPAND | wx.TOP, 0)
        self.bpButtonDelete.SetToolTip("Apagar Treino")

        b_sizer13.Add(b_sizer15, 0, 0, 0)

        sb_sizer_treinamentos_realizados.Add(b_sizer13, 0, wx.BOTTOM | wx.TOP | wx.EXPAND, 5)

        b_sizer_info_projeto.Add(sb_sizer_treinamentos_realizados, 0, wx.EXPAND | wx.LEFT | wx.RIGHT | wx.TOP, 5)

        b_sizer44.Add(b_sizer_info_projeto, 1, wx.EXPAND, 5)

        self.buttonNovoTreinamento = wx.Button(self.panel2, wx.ID_ANY, "Novo Treinamento", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer_info_projeto.Add(self.buttonNovoTreinamento, 0, wx.ALIGN_RIGHT | wx.ALL, 5)
        self.buttonNovoTreinamento.SetToolTip("Novo Treinamento")

        self.panel2.SetSizer(b_sizer44)
        self.panel2.Layout()
        sb_sizer18.Add(self.panel2, 1, wx.EXPAND, 0)

        b_sizer_painel.Add(sb_sizer18, 0, wx.BOTTOM | wx.EXPAND | wx.RIGHT | wx.TOP, 5)

        self.panelMain.SetSizer(b_sizer_painel)
        self.panelMain.Layout()
        b_sizer_frame.Add(self.panelMain, 1, wx.EXPAND, 0)

        self.SetSizer(b_sizer_frame)
        self.Layout()

        self.Centre(wx.BOTH)
        self.Show()

        ##########################################################################################
        # Globais
        ##########################################################################################

        # Listar os Projetos no box lateral
        self._listar_projetos()

        # Chamar o método de detalhesProjeto para carregar as informações do projeto selecionado
        self.listBoxListaProjetos.Bind(wx.EVT_LISTBOX, self._detalhes_projeto)

        ##########################################################################################
        # Configura Tela
        ##########################################################################################

        # Desabilita o botão Apagar projeto
        self.buttonApagarProjeto.Enable(False)

        # Desabilita o botão Novo Treinamento
        self.buttonNovoTreinamento.Enable(False)

        # Desabilita o botão View que vizualisa os detalhes do arquivo cascade
        self.bpButtonView.Enable(False)

        # Desabilita o botão Application que inicia a parte de teste
        self.bpButtonApplication.Enable(False)

        # Desabilita o botão delete que apaga um arquivo cascade
        self.bpButtonDelete.Enable(False)

        ##########################################################################################
        # Cursor
        ##########################################################################################

        self.buttonNovoProjeto.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNovoProjeto.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonApagarProjeto.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonApagarProjeto.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.bpButtonView.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.bpButtonView.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.bpButtonApplication.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.bpButtonApplication.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.bpButtonDelete.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.bpButtonDelete.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonNovoTreinamento.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNovoTreinamento.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################

        # Chama o metodo _novoProjeto para abrir a janela para criar um novo projeto
        self.buttonNovoProjeto.Bind(wx.EVT_BUTTON, self._novo_projeto)

        # Chama o metodo _apagarProjeto para apagar um projeto selecionado
        self.buttonApagarProjeto.Bind(wx.EVT_BUTTON, self._apagar_projeto)

        # Chama o metodo _detalhesCascade para abrir a janela que mostra os detalhes do arquivo cascade selecionado
        self.bpButtonView.Bind(wx.EVT_BUTTON, self._detalhes_cascade)

        # Chama o metodo _realizarTeste para carregar as informações para a realização do teste
        self.listBoxTreinamentos.Bind(wx.EVT_LISTBOX, self._realizar_teste)

        # Chama o metodo _apagarCascade para deletar o cascade selecionado
        self.bpButtonDelete.Bind(wx.EVT_BUTTON, self._apagar_cascade)

        # Chama o metodo _goTreinar para iniciar as configurações de treinamento
        self.buttonNovoTreinamento.Bind(wx.EVT_BUTTON, self._go_treinar)

##########################################################################################
# Get e Set
##########################################################################################

    def get_nome_projeto(self):
        return self._nome_projeto

    def set_nome_projeto(self, nome_projeto: str):
        self._nome_projeto = nome_projeto

##########################################################################################
# Metodos Globais
##########################################################################################

    def _listar_projetos(self):
        """Lista os projetos criados"""
        
        self.listBoxListaProjetos.Clear()
        lista = Projeto().listar_projetos()
        for i in range(len(lista)):
            self.listBoxListaProjetos.Append(lista[i])

    def _apagar_cascade(self, event):
        """"""
        pass

    def _apagar_projeto(self, event, nome_projeto):
        ApagarProjeto(nome_projeto).Show(True)
        self.Close(True)

    def _realizar_teste(self, event):
        """Pegar o nome do arquivo selecionado para enviar ao _go_testar()"""
        # nomeCascade = event.GetString()

        # Habilita os botões
        self.bpButtonView.Enable(True)
        self.bpButtonApplication.Enable(True)
        self.bpButtonDelete.Enable(False)

        self.bpButtonApplication.Bind(wx.EVT_BUTTON, self._go_testar)

    def _detalhes_projeto(self, event):
        """Inserir os dados do Projeto selecionado nos detalhes do projeto"""

        self.buttonApagarProjeto.Enable(True)

        self.set_nome_projeto(event.GetString())
        self.staticTextNomeProjeto.SetLabel(self.get_nome_projeto())

        self.buttonApagarProjeto.Bind(wx.EVT_BUTTON, lambda event: self._apagar_projeto(event, self.get_nome_projeto()))

        # Informa a quantidade de imagens positivas e negativas no projeto
        self.staticTextNumPos.SetLabel(str(Imagem().contar_imagens(self.get_nome_projeto(), "p", "originais")))
        self.staticTextNumNeg.SetLabel(str(Imagem().contar_imagens(self.get_nome_projeto(), "n", "originais")))
        self.staticTextNumImgTeste.SetLabel(str(Imagem().contar_imagens(self.get_nome_projeto(), "t", "originais")))

        # Lista os arquivos de Treinamento que o projeto possui
        self._listar_treinamentos()

        # Habilita o botão novo treinamento
        self.buttonNovoTreinamento.Enable(True)

    def _listar_treinamentos(self):
        """Lista os arquivos xml de treinamento no listBoxTreinamentos pertinentes ao projeto"""

        self.listBoxTreinamentos.Clear()

        lista = Listar().listar_treinamentos(self.get_nome_projeto())

        for l in range(len(lista)):
            self.listBoxTreinamentos.Append(lista[l])

        # Desabilita os botões
        self.bpButtonApplication.Enable(False)
        self.bpButtonDelete.Enable(False)
        self.bpButtonView.Enable(False)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):  # @UnusedVariable
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):  # @UnusedVariable
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _novo_projeto(self, event):  # @UnusedVariable
        """Cria um novo Projeto"""

        NovoProjeto().Show(True)
        self.Close(True)

    def _detalhes_cascade(self, event):  # @UnusedVariable
        """Abre o arquivo contendo os parâmetros usados para a criação do arquivo"""
        
        DetalhesXML(self.get_nome_projeto(), self.listBoxTreinamentos.GetStringSelection()).Show(True)
        self.Close(True)

    def _go_testar(self, event):  # @UnusedVariable
        """Abre a janela Testar"""
        
        Testar(self.get_nome_projeto(), self.listBoxTreinamentos.GetStringSelection()).Show(True)
        self.Close(True)

    def _go_treinar(self, event):  # @UnusedVariable
        """Abre a janela Treinamento"""

        TreinarTipoImagem(self.get_nome_projeto()).Show(True)
        self.Close(True)
