#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.NovoComTreinamento import NovoComTreino
from data.views.NovoSemTreinamento import NovoSemTreino


class RealizarTreinamento(wx.Frame):

    def __init__(self, projeto):

        self._projeto = projeto

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 5-6: Realizar Treinamento | " + projeto.get_nome_projeto(), pos=wx.DefaultPosition, size=wx.Size(350, 140), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer130 = wx.BoxSizer(wx.VERTICAL)

        self.panel12 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer131 = wx.BoxSizer(wx.VERTICAL)

        b_sizer482 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer144 = wx.BoxSizer(wx.VERTICAL)

        self.staticText22 = wx.StaticText(self.panel12, wx.ID_ANY, "Deseja relizar algum Treinamento no projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText22.Wrap(-1)
        b_sizer144.Add(self.staticText22, 0, 0, 0)

        b_sizer96 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticTextNomeProjeto = wx.StaticText(self.panel12, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer96.Add(self.staticTextNomeProjeto, 0, wx.ALIGN_CENTER, 0)

        self.staticText39 = wx.StaticText(self.panel12, wx.ID_ANY, "?", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText39.Wrap(-1)
        b_sizer96.Add(self.staticText39, 0, 0, 0)

        b_sizer144.Add(b_sizer96, 1, wx.ALIGN_CENTER, 5)

        b_sizer482.Add(b_sizer144, 1, wx.EXPAND, 5)

        b_sizer131.Add(b_sizer482, 0, wx.ALIGN_CENTER | wx.ALL, 10)

        b_sizer484 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer487 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer43 = wx.BoxSizer(wx.VERTICAL)

        self.buttonSim = wx.Button(self.panel12, wx.ID_ANY, "Sim", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer43.Add(self.buttonSim, 0, wx.ALIGN_LEFT | wx.RIGHT, 0)

        b_sizer487.Add(b_sizer43, 1, wx.EXPAND, 0)

        b_sizer44 = wx.BoxSizer(wx.VERTICAL)

        self.buttonNao = wx.Button(self.panel12, wx.ID_ANY, "Não", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer44.Add(self.buttonNao, 0, wx.ALIGN_RIGHT | wx.LEFT, 0)

        b_sizer487.Add(b_sizer44, 1, wx.EXPAND, 0)

        b_sizer484.Add(b_sizer487, 1, 0, 5)

        b_sizer131.Add(b_sizer484, 0, wx.ALL | wx.EXPAND, 10)

        self.panel12.SetSizer(b_sizer131)
        self.panel12.Layout()

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        # Aplica o nome do projeto no Label staticTextNomeProjeto
        self.staticTextNomeProjeto.SetLabel(projeto.get_nome_projeto())

        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonNao.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNao.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonSim.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonSim.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################

        # Chama o metodo _goNovoComTreinamento para fechar esta view, criar o projeto e iniciar o treinamento
        self.buttonSim.Bind(wx.EVT_BUTTON, self._go_novo_com_treino)

        # Chama o metodo _goNovoSemTreinamento para fechar esta view, criar o projeto e iniciar o treinamento
        self.buttonNao.Bind(wx.EVT_BUTTON, self._go_novo_sem_treino)

##########################################################################################
# Metodos Globais
##########################################################################################

    def _criar_projeto(self):
        self.Hide()
        resultado = self._projeto.criar_projeto
        return resultado

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_novo_com_treino(self, event):
        if self._criar_projeto():
            NovoComTreino(self._projeto).Show(True)
        else:
            print("Houve algum erro inesperado... Processo finalizado!")

        self.Close(True)

    def _go_novo_sem_treino(self, event):
        if self._criar_projeto():
            NovoSemTreino(self._projeto).Show(True)
        else:
            print("Houve algum erro inesperado... Processo finalizado!")

        self.Close(True)
