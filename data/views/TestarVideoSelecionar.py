#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx

from data.services.Detectar import Detectar
from data.views.TestarVideo import TestarVideo


class TestarVideoSelecionar (wx.Frame):

    def __init__(self, nome_projeto, arquivo_cascade):
        
        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade

        ##########################################################################################
        # Cria Frame
        ##########################################################################################
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar 1-2: Video - Selecionar Vídeo | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(350, 185), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        
        b_sizer1 = wx.BoxSizer(wx.VERTICAL)
        
        self.panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer2 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer3 = wx.BoxSizer(wx.VERTICAL)
        
        self.staticText1 = wx.StaticText(self.panel1, wx.ID_ANY, "Selecione um Vídeo para realizar o teste", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText1.Wrap(-1)
        b_sizer3.Add(self.staticText1, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer2.Add(b_sizer3, 0, wx.ALIGN_CENTER | wx.ALL, 10)
        
        b_sizer67 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.staticText16 = wx.StaticText(self.panel1, wx.ID_ANY, "Vídeo: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText16.Wrap(-1)
        b_sizer67.Add(self.staticText16, 0, wx.BOTTOM | wx.TOP, 5)
        
        self.filePickerVideo = wx.FilePickerCtrl(self.panel1, wx.ID_ANY, wx.EmptyString, "Selecione o arquivo de vídeo", "Vídeos (*.avi, *.wmv, *.mp4): | *.avi; *.wmv; *.mp4", wx.DefaultPosition, wx.Size(220, -1), wx.FLP_FILE_MUST_EXIST | wx.FLP_OPEN | wx.FLP_SMALL | wx.FLP_USE_TEXTCTRL)
        b_sizer67.Add(self.filePickerVideo, 1, 0, 0)
        
        b_sizer2.Add(b_sizer67, 0, wx.ALIGN_CENTER | wx.ALL, 10)
        
        self.staticline3 = wx.StaticLine(self.panel1, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        b_sizer2.Add(self.staticline3, 0, wx.BOTTOM | wx.EXPAND | wx.TOP, 5)
        
        b_sizer6 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer8 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonVoltar = wx.Button(self.panel1, wx.ID_ANY, "< Voltar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer8.Add(self.buttonVoltar, 0, wx.ALIGN_LEFT, 0)
        
        b_sizer6.Add(b_sizer8, 1, wx.EXPAND, 0)
        
        b_sizer9 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonTestar = wx.Button(self.panel1, wx.ID_ANY, "Testar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer9.Add(self.buttonTestar, 0, wx.ALIGN_RIGHT, 0)
        
        b_sizer6.Add(b_sizer9, 1, wx.EXPAND, 0)
        
        b_sizer2.Add(b_sizer6, 0, wx.ALL | wx.EXPAND, 10)
        
        self.panel1.SetSizer(b_sizer2)
        self.panel1.Layout()
        b_sizer1.Add(self.panel1, 1, wx.EXPAND, 0)
        
        self.Centre(wx.BOTH)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonVoltar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonVoltar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonTestar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonTestar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Configura Tela
        ##########################################################################################
        self.buttonTestar.Enable(False)

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        # Chama o método _habilitar_avancar quando o usuário atribuir um valor no campo
        self.filePickerVideo.Bind(wx.EVT_FILEPICKER_CHANGED, self._habilitar_testar)
        
        # Chama o metodo _go_voltar para abrir a view Testar.py e fechar esta view
        self.buttonVoltar.Bind(wx.EVT_BUTTON, self._go_voltar)
        
        # Chama o metodo _go_testar para abrir a view TestarVideo.py e fechar esta view
        self.buttonTestar.Bind(wx.EVT_BUTTON, self._go_testar)

    ##########################################################################################
    # Get e Set
    ##########################################################################################
    def get_nome_projeto(self):
        return self._nomeProjeto

    def get_arquivo_cascade(self):
        return self._arquivoCascade
    
    def get_file_picker_video(self):
        return self.filePickerVideo.GetPath()

    ##########################################################################################
    # Método de Verificação
    ##########################################################################################
    def _habilitar_testar(self, event):
        self.buttonTestar.Enable(True)

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _go_voltar(self, event):
        from data.views.Testar import Testar
        Testar(self.get_nome_projeto(), self.get_arquivo_cascade()).Show(True)
        self.Close(True)

    def _go_testar(self, event):
        # Realizar a detecção no video
        local_video = Detectar().detectar_video(self.get_nome_projeto(), self.get_file_picker_video(), self.get_arquivo_cascade())

        self.Close(True)

        # Mostrar resultado
        TestarVideo(self.get_nome_projeto(), self.get_arquivo_cascade(), local_video).Show(True)
