#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.ResultadoApagar import ResultadoApagar


class ApagarProjeto(wx.Frame):

    def __init__(self, nome_projeto):
        """ Construtor da classe
            :param nome_projeto: Define o nome do projeto
            :type nome_projeto: str
        """

        self._nomeProjeto = nome_projeto

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Apagar Projeto " + nome_projeto + "?",
                          pos=wx.DefaultPosition, size=wx.Size(350, 130), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer13 = wx.BoxSizer(wx.VERTICAL)

        self.panel4 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer14 = wx.BoxSizer(wx.VERTICAL)

        self.staticText7 = wx.StaticText(self.panel4, wx.ID_ANY, "Deseja realmente apagar o projeto",
                                         wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText7.Wrap(-1)
        b_sizer14.Add(self.staticText7, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer15 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticTextNomeProjeto = wx.StaticText(self.panel4, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer15.Add(self.staticTextNomeProjeto, 0, 0, 0)

        self.staticText9 = wx.StaticText(self.panel4, wx.ID_ANY, "?", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText9.Wrap(-1)
        b_sizer15.Add(self.staticText9, 0, 0, 0)

        b_sizer14.Add(b_sizer15, 0, wx.ALIGN_CENTER, 5)

        b_sizer16 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer17 = wx.BoxSizer(wx.VERTICAL)

        self.buttonSim = wx.Button(self.panel4, wx.ID_ANY, "Sim", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer17.Add(self.buttonSim, 0, wx.ALIGN_LEFT, 0)

        b_sizer16.Add(b_sizer17, 1, wx.ALL, 10)

        b_sizer18 = wx.BoxSizer(wx.VERTICAL)

        self.buttonNao = wx.Button(self.panel4, wx.ID_ANY, "Não", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer18.Add(self.buttonNao, 0, wx.ALIGN_RIGHT, 0)

        b_sizer16.Add(b_sizer18, 1, wx.ALL, 10)

        b_sizer14.Add(b_sizer16, 0, wx.ALIGN_CENTER | wx.EXPAND, 5)

        self.panel4.SetSizer(b_sizer14)
        self.panel4.Layout()
        b_sizer13.Add(self.panel4, 1, wx.EXPAND, 0)

        self.SetSizer(b_sizer13)
        self.Layout()

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        self.staticTextNomeProjeto.SetLabel(nome_projeto)

        ##########################################################################################
        # Cursor
        ##########################################################################################

        self.buttonSim.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonSim.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonNao.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonNao.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################

        # Chama o metodo _apagar para abrir a view ResultadoApagar.py e fechar esta view
        self.buttonSim.Bind(wx.EVT_BUTTON, self._apagar)

        # Chama o metodo _nApagar para abrir a view Main.py e fechar esta view
        self.buttonNao.Bind(wx.EVT_BUTTON, self._nao_apagar)

    ##########################################################################################
    # Cursor
    ##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################

    def _apagar(self, event):
        ResultadoApagar(self._nomeProjeto).Show(True)
        self.Close(True)

    def _nao_apagar(self, event):
        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)
