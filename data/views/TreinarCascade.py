# -*- coding: utf-8 -*-

import wx
import os.path
from data.services.Imagem import Imagem
from data.services.Treinamento import Treinamento


class TreinarCascade(wx.Frame):

    def __init__(self, nome_projeto, info, vec, num_pos, filtro, sh, w, h):
        self.nomeProjeto = nome_projeto
        self.info = info
        self.vec = vec
        self.numPos = num_pos
        self.filtro = filtro
        self.numNeg = Imagem().contar_imagens(nome_projeto, "n", "editadas\\" + filtro)
        self.sh = sh
        self.w = w
        self.h = h

        ##########################################################################################
        # Cria Frame
        ##########################################################################################
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Treinamento 3-3: Cascade | " + nome_projeto,
                          pos=wx.DefaultPosition, size=wx.Size(900, 604), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer34 = wx.BoxSizer(wx.VERTICAL)

        self.panel2 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer35 = wx.BoxSizer(wx.VERTICAL)

        sb_sizer11 = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, " 1. Informações Gerais: "), wx.HORIZONTAL)

        b_sizer384 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer1701 = wx.BoxSizer(wx.VERTICAL)

        self.staticText1211 = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "data:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText1211.Wrap(-1)
        b_sizer1701.Add(self.staticText1211, 0, wx.ALL, 5)

        self.staticText1651 = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "bg:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText1651.Wrap(-1)
        b_sizer1701.Add(self.staticText1651, 0, wx.ALL, 5)

        self.staticText1221 = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "vec:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText1221.Wrap(-1)
        b_sizer1701.Add(self.staticText1221, 0, wx.ALL, 5)

        b_sizer384.Add(b_sizer1701, 0, wx.EXPAND, 5)

        b_sizer1731 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextData = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticTextData.Wrap(-1)
        b_sizer1731.Add(self.staticTextData, 0, wx.ALL, 5)

        self.staticTextBg = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize,
                                          0)
        self.staticTextBg.Wrap(-1)
        b_sizer1731.Add(self.staticTextBg, 0, wx.ALL, 5)

        self.staticTextVec = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize,
                                           0)
        self.staticTextVec.Wrap(-1)
        self.staticTextVec.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer1731.Add(self.staticTextVec, 0, wx.ALL, 5)

        b_sizer384.Add(b_sizer1731, 1, wx.EXPAND, 5)

        sb_sizer11.Add(b_sizer384, 1, wx.EXPAND, 5)

        b_sizer385 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer170 = wx.BoxSizer(wx.VERTICAL)

        self.staticText125 = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "w:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.staticText125.Wrap(-1)
        b_sizer170.Add(self.staticText125, 0, wx.ALL, 5)

        self.staticText127 = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "h:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.staticText127.Wrap(-1)
        b_sizer170.Add(self.staticText127, 0, wx.ALL, 5)

        b_sizer385.Add(b_sizer170, 1, wx.EXPAND, 5)

        b_sizer173 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextW = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize,
                                         0)
        self.staticTextW.Wrap(-1)
        self.staticTextW.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer173.Add(self.staticTextW, 0, wx.ALL, 5)

        self.staticTextH = wx.StaticText(sb_sizer11.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize,
                                         0)
        self.staticTextH.Wrap(-1)
        self.staticTextH.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer173.Add(self.staticTextH, 0, wx.ALL, 5)

        b_sizer385.Add(b_sizer173, 7, wx.EXPAND, 5)

        sb_sizer11.Add(b_sizer385, 1, wx.EXPAND, 5)

        b_sizer35.Add(sb_sizer11, 0, wx.ALL | wx.EXPAND, 5)

        b_sizer382 = wx.BoxSizer(wx.HORIZONTAL)

        sb_sizer1 = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, " 2. Parâmetros Gerais: "), wx.VERTICAL)

        b_sizer553 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer573 = wx.BoxSizer(wx.VERTICAL)

        self.staticText773 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "num_pos:", wx.DefaultPosition,
                                           wx.DefaultSize, 0)
        self.staticText773.Wrap(-1)
        b_sizer573.Add(self.staticText773, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer553.Add(b_sizer573, 1, wx.EXPAND, 0)

        b_sizer583 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlNumPos = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        b_sizer583.Add(self.textCtrlNumPos, 0, wx.ALL, 5)

        b_sizer553.Add(b_sizer583, 0, wx.EXPAND, 0)

        b_sizer823 = wx.BoxSizer(wx.VERTICAL)

        self.staticText843 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 85% - " + str(self.numPos),
                                           wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText843.Wrap(-1)
        b_sizer823.Add(self.staticText843, 0, wx.ALL, 8)

        b_sizer553.Add(b_sizer823, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer553, 0, wx.EXPAND, 5)

        b_sizer5531 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer5731 = wx.BoxSizer(wx.VERTICAL)

        self.staticText7731 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "numNeg:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText7731.Wrap(-1)
        b_sizer5731.Add(self.staticText7731, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer5531.Add(b_sizer5731, 1, wx.EXPAND, 0)

        b_sizer5831 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlNumNeg = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                          wx.DefaultSize, 0)
        b_sizer5831.Add(self.textCtrlNumNeg, 0, wx.ALL, 5)

        b_sizer5531.Add(b_sizer5831, 0, wx.EXPAND, 0)

        b_sizer8231 = wx.BoxSizer(wx.VERTICAL)

        self.staticText8431 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 95% - " + str(self.numNeg),
                                            wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText8431.Wrap(-1)
        b_sizer8231.Add(self.staticText8431, 0, wx.ALL, 8)

        b_sizer5531.Add(b_sizer8231, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer5531, 0, wx.EXPAND, 5)

        b_sizer55311 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57311 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77311 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "numStages:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77311.Wrap(-1)
        b_sizer57311.Add(self.staticText77311, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55311.Add(b_sizer57311, 1, wx.EXPAND, 0)

        b_sizer58311 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlNumStage = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        b_sizer58311.Add(self.textCtrlNumStage, 0, wx.ALL, 5)

        b_sizer55311.Add(b_sizer58311, 0, wx.EXPAND, 0)

        b_sizer82311 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84311 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 1", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84311.Wrap(-1)
        b_sizer82311.Add(self.staticText84311, 0, wx.ALL, 8)

        b_sizer55311.Add(b_sizer82311, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55311, 0, wx.EXPAND, 5)

        b_sizer55312 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57312 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77312 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "precalcValBufSize:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText77312.Wrap(-1)
        b_sizer57312.Add(self.staticText77312, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55312.Add(b_sizer57312, 1, wx.EXPAND, 0)

        b_sizer58312 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlPrecalcValBufSize = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58312.Add(self.textCtrlPrecalcValBufSize, 0, wx.ALL, 5)

        b_sizer55312.Add(b_sizer58312, 0, wx.EXPAND, 0)

        b_sizer82312 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84312 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 1024Mb", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84312.Wrap(-1)
        b_sizer82312.Add(self.staticText84312, 0, wx.ALL, 8)

        b_sizer55312.Add(b_sizer82312, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55312, 0, wx.EXPAND, 5)

        b_sizer55313 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57313 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77313 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "precalcIdxBufSize:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText77313.Wrap(-1)
        b_sizer57313.Add(self.staticText77313, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55313.Add(b_sizer57313, 1, wx.EXPAND, 0)

        b_sizer58313 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlPrecalcIdxBufSize = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58313.Add(self.textCtrlPrecalcIdxBufSize, 0, wx.ALL, 5)

        b_sizer55313.Add(b_sizer58313, 0, wx.EXPAND, 0)

        b_sizer82313 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84313 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 1024Mb", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84313.Wrap(-1)
        b_sizer82313.Add(self.staticText84313, 0, wx.ALL, 8)

        b_sizer55313.Add(b_sizer82313, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55313, 0, wx.EXPAND, 5)

        b_sizer55314 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57314 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77314 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "baseFormatSave:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77314.Wrap(-1)
        b_sizer57314.Add(self.staticText77314, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55314.Add(b_sizer57314, 1, wx.EXPAND, 0)

        b_sizer58314 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlBaseFormatSave = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                  wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58314.Add(self.textCtrlBaseFormatSave, 0, wx.ALL, 5)

        b_sizer55314.Add(b_sizer58314, 0, wx.EXPAND, 0)

        b_sizer82314 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84314 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: None", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84314.Wrap(-1)
        b_sizer82314.Add(self.staticText84314, 0, wx.ALL, 8)

        b_sizer55314.Add(b_sizer82314, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55314, 0, wx.EXPAND, 5)

        b_sizer55315 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57315 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77315 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "numThreads:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77315.Wrap(-1)
        b_sizer57315.Add(self.staticText77315, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55315.Add(b_sizer57315, 1, wx.EXPAND, 0)

        b_sizer58315 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlNumThreads = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        b_sizer58315.Add(self.textCtrlNumThreads, 0, wx.ALL, 5)

        b_sizer55315.Add(b_sizer58315, 0, wx.EXPAND, 0)

        b_sizer82315 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84315 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: 1", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84315.Wrap(-1)
        b_sizer82315.Add(self.staticText84315, 0, wx.ALL, 8)

        b_sizer55315.Add(b_sizer82315, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55315, 0, wx.EXPAND, 5)

        b_sizer55316 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57316 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77316 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "acceptanceRatioBreakValue:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText77316.Wrap(-1)
        b_sizer57316.Add(self.staticText77316, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55316.Add(b_sizer57316, 1, wx.EXPAND, 0)

        b_sizer58316 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlAcceptanceRatioBreakValue = wx.TextCtrl(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                             wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58316.Add(self.textCtrlAcceptanceRatioBreakValue, 0, wx.ALL, 5)

        b_sizer55316.Add(b_sizer58316, 0, wx.EXPAND, 0)

        b_sizer82316 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84316 = wx.StaticText(sb_sizer1.GetStaticBox(), wx.ID_ANY, "default: -1", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84316.Wrap(-1)
        b_sizer82316.Add(self.staticText84316, 0, wx.ALL, 8)

        b_sizer55316.Add(b_sizer82316, 1, wx.EXPAND, 5)

        sb_sizer1.Add(b_sizer55316, 0, wx.EXPAND, 5)

        b_sizer382.Add(sb_sizer1, 1, wx.ALL | wx.EXPAND, 5)

        sb_sizer3 = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, " 3. Parâmetros de Boosted: "), wx.VERTICAL)

        b_sizer55331 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57331 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77331 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "bt:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77331.Wrap(-1)
        b_sizer57331.Add(self.staticText77331, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55331.Add(b_sizer57331, 1, wx.EXPAND, 0)

        b_sizer58331 = wx.BoxSizer(wx.VERTICAL)

        choice_bt_choices = ["DAB", "RAB", "LB", "GAB"]
        self.choiceBt = wx.Choice(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(110, -1),
                                  choice_bt_choices, 0)
        self.choiceBt.SetSelection(3)
        b_sizer58331.Add(self.choiceBt, 0, wx.ALL, 5)

        b_sizer55331.Add(b_sizer58331, 0, wx.EXPAND, 0)

        b_sizer82331 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84331 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: GAB", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84331.Wrap(-1)
        b_sizer82331.Add(self.staticText84331, 0, wx.ALL, 8)

        b_sizer55331.Add(b_sizer82331, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55331, 0, wx.EXPAND, 5)

        b_sizer55332 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57332 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77332 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "minHitRate", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77332.Wrap(-1)
        b_sizer57332.Add(self.staticText77332, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55332.Add(b_sizer57332, 1, wx.EXPAND, 0)

        b_sizer58332 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlMinHitRate = wx.TextCtrl(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        b_sizer58332.Add(self.textCtrlMinHitRate, 0, wx.ALL, 5)

        b_sizer55332.Add(b_sizer58332, 0, wx.EXPAND, 0)

        b_sizer82332 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84332 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: 0.995", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84332.Wrap(-1)
        b_sizer82332.Add(self.staticText84332, 0, wx.ALL, 8)

        b_sizer55332.Add(b_sizer82332, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55332, 0, wx.EXPAND, 5)

        b_sizer55333 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57333 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77333 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "maxFalseAlarmRate:",
                                             wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText77333.Wrap(-1)
        b_sizer57333.Add(self.staticText77333, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55333.Add(b_sizer57333, 1, wx.EXPAND, 0)

        b_sizer58333 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlMaxFalseAlarmRate = wx.TextCtrl(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                     wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58333.Add(self.textCtrlMaxFalseAlarmRate, 0, wx.ALL, 5)

        b_sizer55333.Add(b_sizer58333, 0, wx.EXPAND, 0)

        b_sizer82333 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84333 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: 0.5", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84333.Wrap(-1)
        b_sizer82333.Add(self.staticText84333, 0, wx.ALL, 8)

        b_sizer55333.Add(b_sizer82333, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55333, 0, wx.EXPAND, 5)

        b_sizer55334 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57334 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77334 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "weightTrimRate:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77334.Wrap(-1)
        b_sizer57334.Add(self.staticText77334, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55334.Add(b_sizer57334, 1, wx.EXPAND, 0)

        b_sizer58334 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlWeightTrimRate = wx.TextCtrl(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString,
                                                  wx.DefaultPosition, wx.DefaultSize, 0)
        b_sizer58334.Add(self.textCtrlWeightTrimRate, 0, wx.ALL, 5)

        b_sizer55334.Add(b_sizer58334, 0, wx.EXPAND, 0)

        b_sizer82334 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84334 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: 0.95", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84334.Wrap(-1)
        b_sizer82334.Add(self.staticText84334, 0, wx.ALL, 8)

        b_sizer55334.Add(b_sizer82334, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55334, 0, wx.EXPAND, 5)

        b_sizer55335 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57335 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77335 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "maxDepth:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77335.Wrap(-1)
        b_sizer57335.Add(self.staticText77335, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55335.Add(b_sizer57335, 1, wx.EXPAND, 0)

        b_sizer58335 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlMaxDepth = wx.TextCtrl(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        b_sizer58335.Add(self.textCtrlMaxDepth, 0, wx.ALL, 5)

        b_sizer55335.Add(b_sizer58335, 0, wx.EXPAND, 0)

        b_sizer82335 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84335 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: 1", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84335.Wrap(-1)
        b_sizer82335.Add(self.staticText84335, 0, wx.ALL, 8)

        b_sizer55335.Add(b_sizer82335, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55335, 0, wx.EXPAND, 5)

        b_sizer55336 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer57336 = wx.BoxSizer(wx.VERTICAL)

        self.staticText77336 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "maxWeakCount:", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText77336.Wrap(-1)
        b_sizer57336.Add(self.staticText77336, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer55336.Add(b_sizer57336, 1, wx.EXPAND, 0)

        b_sizer58336 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlMaxWeakCount = wx.TextCtrl(sb_sizer3.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                                wx.DefaultSize, 0)
        b_sizer58336.Add(self.textCtrlMaxWeakCount, 0, wx.ALL, 5)

        b_sizer55336.Add(b_sizer58336, 0, wx.EXPAND, 0)

        b_sizer82336 = wx.BoxSizer(wx.VERTICAL)

        self.staticText84336 = wx.StaticText(sb_sizer3.GetStaticBox(), wx.ID_ANY, "default: 100", wx.DefaultPosition,
                                             wx.DefaultSize, 0)
        self.staticText84336.Wrap(-1)
        b_sizer82336.Add(self.staticText84336, 0, wx.ALL, 8)

        b_sizer55336.Add(b_sizer82336, 1, wx.EXPAND, 5)

        sb_sizer3.Add(b_sizer55336, 0, wx.EXPAND, 5)

        b_sizer382.Add(sb_sizer3, 1, wx.ALL | wx.EXPAND, 5)

        b_sizer35.Add(b_sizer382, 0, wx.EXPAND, 5)

        b_sizer383 = wx.BoxSizer(wx.HORIZONTAL)

        sb_sizer2 = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, " 4. Parâmetros Cascade: "), wx.VERTICAL)

        b_sizer5532 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer5732 = wx.BoxSizer(wx.VERTICAL)

        self.staticText7732 = wx.StaticText(sb_sizer2.GetStaticBox(), wx.ID_ANY, "stageType:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText7732.Wrap(-1)
        b_sizer5732.Add(self.staticText7732, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer5532.Add(b_sizer5732, 1, wx.EXPAND, 0)

        b_sizer5832 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlStageType = wx.TextCtrl(sb_sizer2.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                                             wx.DefaultSize, 0)

        b_sizer5832.Add(self.textCtrlStageType, 0, wx.ALL, 5)

        b_sizer5532.Add(b_sizer5832, 0, wx.EXPAND, 0)

        b_sizer8232 = wx.BoxSizer(wx.VERTICAL)

        self.staticText8432 = wx.StaticText(sb_sizer2.GetStaticBox(), wx.ID_ANY, "default: BOOST", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText8432.Wrap(-1)
        b_sizer8232.Add(self.staticText8432, 0, wx.ALL, 8)

        b_sizer5532.Add(b_sizer8232, 1, wx.EXPAND, 5)

        sb_sizer2.Add(b_sizer5532, 0, wx.EXPAND, 5)

        b_sizer5533 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer5733 = wx.BoxSizer(wx.VERTICAL)

        self.staticText7733 = wx.StaticText(sb_sizer2.GetStaticBox(), wx.ID_ANY, "featureType:", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText7733.Wrap(-1)
        b_sizer5733.Add(self.staticText7733, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer5533.Add(b_sizer5733, 1, wx.EXPAND, 0)

        b_sizer5833 = wx.BoxSizer(wx.VERTICAL)

        choice_feature_type_choices = ["HAAR", "LBP"]
        self.choiceFeatureType = wx.Choice(sb_sizer2.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(110, -1),
                                           choice_feature_type_choices, 0)
        self.choiceFeatureType.SetSelection(0)
        b_sizer5833.Add(self.choiceFeatureType, 0, wx.ALL, 5)

        b_sizer5533.Add(b_sizer5833, 0, wx.EXPAND, 0)

        b_sizer8233 = wx.BoxSizer(wx.VERTICAL)

        self.staticText8433 = wx.StaticText(sb_sizer2.GetStaticBox(), wx.ID_ANY, "default: HAAR", wx.DefaultPosition,
                                            wx.DefaultSize, 0)
        self.staticText8433.Wrap(-1)
        b_sizer8233.Add(self.staticText8433, 0, wx.ALL, 8)

        b_sizer5533.Add(b_sizer8233, 1, wx.EXPAND, 5)

        sb_sizer2.Add(b_sizer5533, 0, wx.EXPAND, 5)

        b_sizer383.Add(sb_sizer2, 1, wx.ALL | wx.EXPAND, 5)

        sb_sizer4 = wx.StaticBoxSizer(wx.StaticBox(self.panel2, wx.ID_ANY, " 5. Parâmetros Haar: "), wx.VERTICAL)

        b_sizer553361 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer573361 = wx.BoxSizer(wx.VERTICAL)

        self.staticText773361 = wx.StaticText(sb_sizer4.GetStaticBox(), wx.ID_ANY, "mode:", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.staticText773361.Wrap(-1)
        b_sizer573361.Add(self.staticText773361, 0, wx.ALIGN_RIGHT | wx.BOTTOM | wx.TOP, 8)

        b_sizer553361.Add(b_sizer573361, 1, wx.EXPAND, 0)

        b_sizer583361 = wx.BoxSizer(wx.VERTICAL)

        choice_mode_choices = ["BASIC", "CORE", "ALL"]
        self.choiceMode = wx.Choice(sb_sizer4.GetStaticBox(), wx.ID_ANY, wx.DefaultPosition, wx.Size(110, -1),
                                    choice_mode_choices, 0)
        self.choiceMode.SetSelection(0)
        b_sizer583361.Add(self.choiceMode, 0, wx.ALL, 5)

        b_sizer553361.Add(b_sizer583361, 0, wx.EXPAND, 0)

        b_sizer823361 = wx.BoxSizer(wx.VERTICAL)

        self.staticText843361 = wx.StaticText(sb_sizer4.GetStaticBox(), wx.ID_ANY, "default: BASIC", wx.DefaultPosition,
                                              wx.DefaultSize, 0)
        self.staticText843361.Wrap(-1)
        b_sizer823361.Add(self.staticText843361, 0, wx.ALL, 8)

        b_sizer553361.Add(b_sizer823361, 1, wx.EXPAND, 5)

        sb_sizer4.Add(b_sizer553361, 0, wx.EXPAND, 5)

        b_sizer383.Add(sb_sizer4, 1, wx.ALL | wx.EXPAND, 5)

        b_sizer35.Add(b_sizer383, 0, wx.EXPAND, 5)

        b_sizer43 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer45 = wx.BoxSizer(wx.VERTICAL)

        self.buttonVoltar = wx.Button(self.panel2, wx.ID_ANY, "< Voltar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer45.Add(self.buttonVoltar, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer43.Add(b_sizer45, 1, 0, 0)

        b_sizer46 = wx.BoxSizer(wx.VERTICAL)

        self.buttonCancelar = wx.Button(self.panel2, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer46.Add(self.buttonCancelar, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer43.Add(b_sizer46, 1, 0, 0)

        b_sizer451 = wx.BoxSizer(wx.VERTICAL)

        self.buttonTreinar = wx.Button(self.panel2, wx.ID_ANY, "Treinar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer451.Add(self.buttonTreinar, 0, wx.ALIGN_CENTER | wx.ALL, 5)

        b_sizer43.Add(b_sizer451, 1, wx.EXPAND, 5)

        b_sizer35.Add(b_sizer43, 0, wx.ALL | wx.EXPAND, 5)

        self.panel2.SetSizer(b_sizer35)
        self.panel2.Layout()
        b_sizer34.Add(self.panel2, 0, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################
        self.textCtrlStageType.Enable(False)
        self.textCtrlBaseFormatSave.Enable(False)

        self.staticTextData.SetLabel(os.path.join(nome_projeto, 'cascade'))
        self.staticTextBg.SetLabel(
            os.path.join(nome_projeto, 'negativas', 'editadas', str('negativas-' + filtro + '.txt')))
        self.staticTextVec.SetLabel(vec)

        self.staticTextW.SetLabel(str(w))
        self.staticTextH.SetLabel(str(h))

        valor = int(float(self.numPos) * 0.85) + 1
        self.textCtrlNumPos.SetValue(str(valor))

        valor = int(float(self.numNeg) * 0.95) + 1
        self.textCtrlNumNeg.SetValue(str(valor))

        self.textCtrlNumStage.SetValue("1")

        self.textCtrlPrecalcValBufSize.SetValue("1024")
        self.textCtrlPrecalcIdxBufSize.SetValue("1024")
        self.textCtrlBaseFormatSave.SetValue("None")
        self.textCtrlNumThreads.SetValue("1")
        self.textCtrlAcceptanceRatioBreakValue.SetValue("-1")

        self.textCtrlMinHitRate.SetValue("0.995")
        self.textCtrlMaxFalseAlarmRate.SetValue("0.5")
        self.textCtrlWeightTrimRate.SetValue("0.95")
        self.textCtrlMaxDepth.SetValue("1")
        self.textCtrlMaxWeakCount.SetValue("100")

        self.textCtrlStageType.SetValue("BOOST")

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonVoltar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonVoltar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        self.buttonVoltar.SetToolTip("Voltar")

        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        self.buttonCancelar.SetToolTip("Cancelar")

        self.buttonTreinar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonTreinar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        self.buttonTreinar.SetToolTip("Treinar")

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        self.buttonVoltar.Bind(wx.EVT_BUTTON, self._voltar)
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._cancelar)
        self.buttonTreinar.Bind(wx.EVT_BUTTON, self._treinar)

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _voltar(self, event):
        from data.views.TreinarSample import TreinarSample
        TreinarSample(self.nomeProjeto, self.filtro, self.w, self.h).Show(True)
        self.Close(True)

    def _cancelar(self, event):
        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)

    def _treinar(self, event):
        print("===========================================")
        print("=======: Criação do arquivo SAMPLE :=======")
        print("===========================================")
        self.sample = [self.info, self.vec, self.numPos, self.sh, self.staticTextW.GetLabel(),
                       self.staticTextH.GetLabel()]
        Treinamento(self.nomeProjeto, 's', self.sample)

        print("============================================")
        print("=======: Criação do arquivo CASCADE :=======")
        print("============================================")
        self.treinamento = [str(
            os.path.join(self.staticTextData.GetLabelText(), self.choiceFeatureType.GetStringSelection(),
                         self.choiceBt.GetStringSelection(), self.filtro, self.textCtrlNumStage.GetValue())),
                            str(self.staticTextBg.GetLabelText()),
                            str(self.staticTextVec.GetLabelText()),
                            str(self.staticTextW.GetLabel()),
                            str(self.staticTextH.GetLabel()),
                            str(self.textCtrlNumPos.GetValue()),
                            str(self.textCtrlNumNeg.GetValue()),
                            str(self.textCtrlNumStage.GetValue()),
                            str(self.textCtrlPrecalcValBufSize.GetValue()),
                            str(self.textCtrlPrecalcIdxBufSize.GetValue()),
                            str(self.textCtrlBaseFormatSave.GetValue()),
                            str(self.textCtrlNumThreads.GetValue()),
                            str(self.textCtrlAcceptanceRatioBreakValue.GetValue()),
                            str(self.choiceBt.GetStringSelection()),
                            str(self.textCtrlMinHitRate.GetValue()),
                            str(self.textCtrlMaxFalseAlarmRate.GetValue()),
                            str(self.textCtrlWeightTrimRate.GetValue()),
                            str(self.textCtrlMaxDepth.GetValue()),
                            str(self.textCtrlMaxWeakCount.GetValue()),
                            str(self.textCtrlStageType.GetValue()),
                            str(self.choiceFeatureType.GetStringSelection()),
                            str(self.choiceMode.GetStringSelection())]

        Treinamento(self.nomeProjeto, 't', self.treinamento)

        self._cancelar(event)
