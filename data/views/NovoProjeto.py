#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.services.Projeto import Projeto
from data.views.ProjetoExiste import ProjetoExiste
from data.views.SelecionarImagens import SelecionarImagens


class NovoProjeto (wx.Frame):

    def __init__(self):

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 1-6: Nome do Projeto", pos=wx.DefaultPosition, size=wx.Size(350, 168), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer_frame = wx.BoxSizer(wx.VERTICAL)

        self.panelMain = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer486 = wx.BoxSizer(wx.VERTICAL)

        b_sizer112 = wx.BoxSizer(wx.VERTICAL)

        self.staticText33 = wx.StaticText(self.panelMain, wx.ID_ANY, "Informe o nome do qual o projeto deverá identificado", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText33.Wrap(-1)
        b_sizer112.Add(self.staticText33, 0, wx.ALIGN_CENTER, 0)

        b_sizer486.Add(b_sizer112, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer482 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer41 = wx.BoxSizer(wx.VERTICAL)

        self.staticText295 = wx.StaticText(self.panelMain, wx.ID_ANY, "Nome do Projeto:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText295.Wrap(-1)
        b_sizer41.Add(self.staticText295, 0, wx.BOTTOM | wx.RIGHT | wx.TOP, 3)

        b_sizer482.Add(b_sizer41, 0, 0, 0)

        b_sizer42 = wx.BoxSizer(wx.VERTICAL)

        self.textCtrlNomeProjeto = wx.TextCtrl(self.panelMain, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(200, -1), 0)
        b_sizer42.Add(self.textCtrlNomeProjeto, 1, 0, 0)

        b_sizer482.Add(b_sizer42, 0, wx.EXPAND, 0)

        b_sizer486.Add(b_sizer482, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer484 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer487 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer43 = wx.BoxSizer(wx.VERTICAL)

        self.buttonCancelar = wx.Button(self.panelMain, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer43.Add(self.buttonCancelar, 0, wx.ALIGN_LEFT | wx.RIGHT, 0)

        b_sizer487.Add(b_sizer43, 1, wx.EXPAND, 5)

        b_sizer44 = wx.BoxSizer(wx.VERTICAL)

        self.buttonAvancar = wx.Button(self.panelMain, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer44.Add(self.buttonAvancar, 0, wx.ALIGN_CENTER | wx.ALIGN_RIGHT, 0)

        b_sizer487.Add(b_sizer44, 1, wx.EXPAND, 5)

        b_sizer484.Add(b_sizer487, 1, 0, 5)

        b_sizer486.Add(b_sizer484, 0, wx.ALL | wx.EXPAND, 10)

        self.panelMain.SetSizer(b_sizer486)
        self.panelMain.Layout()
        b_sizer_frame.Add(self.panelMain, 0, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################

        # Verificar se foi informado algum texto para habilitar o botão avançar
        self.textCtrlNomeProjeto.Bind(wx.EVT_TEXT, self._habilitar_avancar)

        ##########################################################################################
        # Configura Tela
        ##########################################################################################

        # Desabilita o botão continuar
        self.buttonAvancar.Enable(False)

        ##########################################################################################
        # Cursor
        ##########################################################################################

        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################

        # Chama o metodo _goSelecionarImagens para abrir a view SelecionarImagem.py
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._go_selecionar_imagens)

        # Chama o metodo _fechar para fechar a view atual
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._fechar)

##########################################################################################
# Metodos Globais
##########################################################################################

    def _habilitar_avancar(self, event):
        self.buttonAvancar.Enable(True)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_selecionar_imagens(self, event):

        projeto = Projeto()
        continuar = projeto.set_nome_projeto(self.textCtrlNomeProjeto.GetValue())

        if continuar:
            self.Close(True)
            SelecionarImagens(projeto).Show(True)
        else:
            self.Close(True)
            ProjetoExiste(projeto).Show(True)

    def _fechar(self, event):
        self.Close(True)
        from data.views.Main import Main
        Main().Show(True)
