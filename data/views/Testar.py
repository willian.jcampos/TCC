#!/usr/bin/env python
# -*- coding: utf-8 -*-

import wx
from data.views.TestarImagensSelecionar import TestarImagensSelecionar
from data.views.TestarVideoSelecionar import TestarVideoSelecionar


class Testar(wx.Frame):

    def __init__(self, nome_projeto, arquivo_cascade):

        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################

        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar: Selecionar Tipo de Teste | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(450, 140), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer1 = wx.BoxSizer(wx.VERTICAL)

        self.panel1 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer2 = wx.BoxSizer(wx.VERTICAL)

        b_sizer3 = wx.BoxSizer(wx.VERTICAL)

        self.staticText1 = wx.StaticText(self.panel1, wx.ID_ANY, "Selecione o tipo de mídia para realizar o teste", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText1.Wrap(-1)
        b_sizer3.Add(self.staticText1, 0, wx.ALIGN_CENTER, 0)

        b_sizer91 = wx.BoxSizer(wx.HORIZONTAL)

        self.staticText26 = wx.StaticText(self.panel1, wx.ID_ANY, "no projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText26.Wrap(-1)
        b_sizer91.Add(self.staticText26, 0, 0, 0)

        self.staticTextNomeProjeto = wx.StaticText(self.panel1, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer91.Add(self.staticTextNomeProjeto, 0, 0, 0)

        self.staticText28 = wx.StaticText(self.panel1, wx.ID_ANY, ".", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText28.Wrap(-1)
        b_sizer91.Add(self.staticText28, 0, 0, 0)

        b_sizer3.Add(b_sizer91, 0, wx.ALIGN_CENTER, 0)

        b_sizer2.Add(b_sizer3, 0, wx.ALL | wx.EXPAND, 10)

        b_sizer6 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer8 = wx.BoxSizer(wx.VERTICAL)

        self.buttonVideo = wx.Button(self.panel1, wx.ID_ANY, "Vídeo", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer8.Add(self.buttonVideo, 0, wx.ALIGN_LEFT, 0)

        b_sizer6.Add(b_sizer8, 1, wx.EXPAND, 0)

        b_sizer9 = wx.BoxSizer(wx.VERTICAL)

        self.buttonImagens = wx.Button(self.panel1, wx.ID_ANY, "Imagens", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer9.Add(self.buttonImagens, 0, wx.ALIGN_CENTER, 0)

        b_sizer6.Add(b_sizer9, 1, wx.EXPAND, 0)

        b_sizer57 = wx.BoxSizer(wx.VERTICAL)

        self.buttonCancelar = wx.Button(self.panel1, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer57.Add(self.buttonCancelar, 0, wx.ALIGN_RIGHT, 0)

        b_sizer6.Add(b_sizer57, 1, 0, 0)

        b_sizer2.Add(b_sizer6, 0, wx.ALL | wx.EXPAND, 10)

        self.panel1.SetSizer(b_sizer2)
        self.panel1.Layout()
        b_sizer1.Add(self.panel1, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Geral
        ##########################################################################################
        
        self.staticTextNomeProjeto.SetLabel(self._nomeProjeto)

        ##########################################################################################
        # Cursor
        ##########################################################################################

        self.buttonVideo.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonVideo.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonImagens.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonImagens.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de desparo
        ##########################################################################################

        # Chama o metodo _goTestarVideoSelecionar para abrir a view TestarVideoSelecionar.py e fechar esta view
        self.buttonVideo.Bind(wx.EVT_BUTTON, self._go_testar_video_selecionar)

        # Chama o metodo _goTestarImagensSelecionar para abrir a view TestarImagensSelecionar.py e fechar esta view
        self.buttonImagens.Bind(wx.EVT_BUTTON, self._go_testar_imagens_selecionar)
        
        # Chama o metodo _goMain para abrir a view Main.py e fechar esta view
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._go_main)

##########################################################################################
# Get e Set
##########################################################################################

    def get_nome_projeto(self):
        return self._nomeProjeto

    def get_arquivo_cascade(self):
        return self._arquivoCascade

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_testar_video_selecionar(self, event):
        """Método responsável por fechar esta janela e abrir a janela TestarVideoBiblioteca"""

        TestarVideoSelecionar(self.get_nome_projeto(), self.get_arquivo_cascade()).Show(True)
        self.Close(True)

    def _go_testar_imagens_selecionar(self, event):
        """Método responsável por fechar esta janela e abrir a janela TestarSelecionarImagem"""

        TestarImagensSelecionar(self.get_nome_projeto(), self.get_arquivo_cascade()).Show(True)
        self.Close(True)

    def _go_main(self, event):
        """Método responsável por fechar esta janela e abrir a janela TestarSelecionarImagem"""

        from data.views.Main import Main
        Main().Show(True)
        
        self.Close(True)
