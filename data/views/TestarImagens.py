#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os.path
import numpy
import wx
from data.services.Arquivo import Arquivo
from data.services.Detectar import Detectar
from data.services.Imagem import Imagem
from data.services.Listar import Listar
from data.util.Locais import Locais
from data.views.TestarImagensResultado import TestarImagensResultado


class TestarImagens(wx.Frame):
    """Classe responsivel por criar a janela TestarImagens"""

    def __init__(self, nome_projeto, arquivo_cascade, local):
        """Cria a janela TestarImagens e recebe o nome do projeto e o arquivo cascade
            :param nome_projeto: nome do projeto
            :type nome_projeto: str

            :param arquivo_cascade: local onde se encontra o arquivo cascade
            :type arquivo_cascade: str

            :param local: local onde as imagens se encontram
            :type local: str
        """

        self._nomeProjeto = nome_projeto
        self._arquivoCascade = arquivo_cascade
        self._local = local

        self._listaImagensOriginais = ""
        self._numeroTotalImagens = 0

        self._numeroImagemAtual = 1

        self._localImagensOriginaisTeste = Locais().pasta_teste_imagens_originais(nome_projeto)
        self._localImagensDetectadasTeste = Locais().pasta_teste_imagens_detectadas(nome_projeto)

        self._info = 0

        self.qtdDetectada = 0

        self._iniciar()

        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Testar 2-3: Imagens | " + nome_projeto, pos=wx.DefaultPosition, size=wx.Size(900, 715), style=wx.CAPTION | wx.TAB_TRAVERSAL)

        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)

        b_sizer65 = wx.BoxSizer(wx.VERTICAL)

        self.panel9 = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer66 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer_imagens = wx.BoxSizer(wx.VERTICAL)

        sb_sizer1 = wx.StaticBoxSizer(wx.StaticBox(self.panel9, wx.ID_ANY, "Original"), wx.VERTICAL)
        img_original = wx.Image(500, 300)
        self.bitmapOriginal = wx.StaticBitmap(sb_sizer1.GetStaticBox(), wx.ID_ANY, wx.Bitmap(img_original))
        sb_sizer1.Add(self.bitmapOriginal, 0, wx.ALL, 5)

        b_sizer_imagens.Add(sb_sizer1, 1, wx.EXPAND, 5)

        sb_sizer2 = wx.StaticBoxSizer(wx.StaticBox(self.panel9, wx.ID_ANY, "Detectada: " + arquivo_cascade), wx.VERTICAL)
        img_detectada = wx.Image(500, 300)
        self.bitmapDetectado = wx.StaticBitmap(sb_sizer2.GetStaticBox(), wx.ID_ANY, wx.Bitmap(img_detectada))
        sb_sizer2.Add(self.bitmapDetectado, 0, wx.ALL, 5)

        b_sizer_imagens.Add(sb_sizer2, 1, wx.EXPAND, 5)

        b_sizer66.Add(b_sizer_imagens, 0, wx.ALL | wx.EXPAND, 5)

        b_sizer_info = wx.BoxSizer(wx.VERTICAL)

        sb_sizer_info_imagem = wx.StaticBoxSizer(wx.StaticBox(self.panel9, wx.ID_ANY, "Informações da Imagem"), wx.HORIZONTAL)

        b_sizer73 = wx.BoxSizer(wx.VERTICAL)

        self.staticText31 = wx.StaticText(sb_sizer_info_imagem.GetStaticBox(), wx.ID_ANY, "Nome: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText31.Wrap(-1)
        b_sizer73.Add(self.staticText31, 0, 0, 0)

        self.staticText32 = wx.StaticText(sb_sizer_info_imagem.GetStaticBox(), wx.ID_ANY, "Tamanho: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText32.Wrap(-1)
        b_sizer73.Add(self.staticText32, 0, 0, 0)

        sb_sizer_info_imagem.Add(b_sizer73, 0, wx.BOTTOM | wx.EXPAND | wx.LEFT | wx.TOP, 5)

        b_sizer74 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextNomeImagem = wx.StaticText(sb_sizer_info_imagem.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeImagem.Wrap(-1)
        self.staticTextNomeImagem.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer74.Add(self.staticTextNomeImagem, 0, 0, 0)

        self.staticTextTamanho = wx.StaticText(sb_sizer_info_imagem.GetStaticBox(), wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextTamanho.Wrap(-1)
        self.staticTextTamanho.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer74.Add(self.staticTextTamanho, 0, 0, 0)

        sb_sizer_info_imagem.Add(b_sizer74, 0, wx.BOTTOM | wx.EXPAND | wx.RIGHT | wx.TOP, 5)

        b_sizer_info.Add(sb_sizer_info_imagem, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_info_detect = wx.StaticBoxSizer(wx.StaticBox(self.panel9, wx.ID_ANY, "Informações da Detecção"), wx.VERTICAL)

        b_sizer83 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer76 = wx.BoxSizer(wx.VERTICAL)

        self.staticText47 = wx.StaticText(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, "Detectado: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText47.Wrap(-1)
        b_sizer76.Add(self.staticText47, 0, wx.BOTTOM, 20)

        self.staticText38 = wx.StaticText(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, "Positivo:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText38.Wrap(-1)
        b_sizer76.Add(self.staticText38, 0, wx.BOTTOM | wx.TOP, 5)

        self.staticText39 = wx.StaticText(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, "Falso-Positivo:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText39.Wrap(-1)
        b_sizer76.Add(self.staticText39, 0, wx.BOTTOM | wx.TOP, 5)

        self.staticText43 = wx.StaticText(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, "Falso-Negativo:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText43.Wrap(-1)
        b_sizer76.Add(self.staticText43, 0, wx.BOTTOM | wx.TOP, 5)

        b_sizer83.Add(b_sizer76, 0, wx.ALL | wx.EXPAND, 5)

        b_sizer77 = wx.BoxSizer(wx.VERTICAL)

        self.staticTextNObjetosDetectados = wx.StaticText(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, "00", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNObjetosDetectados.Wrap(-1)
        self.staticTextNObjetosDetectados.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))

        b_sizer77.Add(self.staticTextNObjetosDetectados, 0, wx.BOTTOM, 20)

        self.nObj = wx.TextCtrl(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(50, -1), 0)
        b_sizer77.Add(self.nObj, 0, wx.BOTTOM | wx.TOP, 2)

        self.fp = wx.TextCtrl(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.Size(50, -1), 0)
        b_sizer77.Add(self.fp, 0, wx.BOTTOM | wx.TOP, 2)

        self.fn = wx.TextCtrl(sb_sizer_info_detect.GetStaticBox(), wx.ID_ANY, wx.EmptyString, wx.DefaultPosition,
                              wx.Size(50, -1), 0)
        b_sizer77.Add(self.fn, 0, wx.BOTTOM | wx.TOP, 2)

        b_sizer83.Add(b_sizer77, 0, wx.ALL | wx.EXPAND, 5)

        sb_sizer_info_detect.Add(b_sizer83, 0, wx.EXPAND, 5)

        b_sizer_info.Add(sb_sizer_info_detect, 0, wx.ALL | wx.EXPAND, 5)

        self.staticline8 = wx.StaticLine(self.panel9, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL)
        b_sizer_info.Add(self.staticline8, 0, wx.EXPAND | wx.TOP, 350)

        b_sizer70 = wx.BoxSizer(wx.HORIZONTAL)

        b_sizer88 = wx.BoxSizer(wx.VERTICAL)

        self.buttonSair = wx.Button(self.panel9, wx.ID_ANY, "Sair", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer88.Add(self.buttonSair, 0, 0, 0)

        b_sizer70.Add(b_sizer88, 1, wx.EXPAND, 5)

        b_sizer89 = wx.BoxSizer(wx.VERTICAL)

        self.buttonProxima = wx.Button(self.panel9, wx.ID_ANY, "Imagem " + str(self.get_numero_imagem_atual()) + " de " + str(self.get_numero_total_imagens()) + " >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer89.Add(self.buttonProxima, 0, wx.ALIGN_RIGHT | wx.BOTTOM, 3)

        self.buttonResultado = wx.Button(self.panel9, wx.ID_ANY, "Resultado", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer89.Add(self.buttonResultado, 0, wx.ALIGN_RIGHT | wx.TOP, 3)

        b_sizer70.Add(b_sizer89, 1, wx.EXPAND, 5)

        b_sizer_info.Add(b_sizer70, 0, wx.ALIGN_BOTTOM | wx.ALIGN_RIGHT | wx.ALL | wx.EXPAND, 5)

        b_sizer66.Add(b_sizer_info, 1, wx.ALL | wx.EXPAND, 0)

        self.panel9.SetSizer(b_sizer66)
        self.panel9.Layout()
        b_sizer65.Add(self.panel9, 1, wx.EXPAND, 0)

        self.Centre(wx.BOTH)

        ##########################################################################################
        # Globais
        ##########################################################################################
        self._carregar_imagem()

        self.buttonResultado.Enable(False)

        self._info = numpy.zeros((self.get_numero_total_imagens(), 4))

        ##########################################################################################
        # Cursor
        ##########################################################################################
        self.buttonProxima.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonProxima.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonResultado.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonResultado.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        self.buttonSair.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonSair.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de Disparo
        ##########################################################################################
        # Chama o método _proxima_imagem para trocar a imagem da tela
        self.buttonProxima.Bind(wx.EVT_BUTTON, self._proxima_imagem)

        # Chama o método _go_resultado para exibir a tela TestarImagemResultado
        self.buttonResultado.Bind(wx.EVT_BUTTON, self._go_resultado)

        # Chama o método _sair para exibir a tela Main
        self.buttonSair.Bind(wx.EVT_BUTTON, self._sair)

    ##########################################################################################
    # Get e Set
    ##########################################################################################
    def get_nome_projeto(self):
        return self._nomeProjeto

    def get_arquivo_cascade(self):
        return self._arquivoCascade

    def get_numero_imagem_atual(self):
        return self._numeroImagemAtual

    def set_numero_imagem_atual(self, numero_imagem_atual):
        self._numeroImagemAtual = numero_imagem_atual

    def get_numero_total_imagens(self):
        return self._numeroTotalImagens

    def get_local(self):
        return self._local

    def get_lista_imagens_originais(self):
        return self._listaImagensOriginais

    ##########################################################################################
    # Locais
    ##########################################################################################
    def _iniciar(self):
        # Apagar as imagens da pasta teste
        Arquivo().apagar_imagens_teste(self.get_nome_projeto())

        # Faz a copia das imagens de teste para a pasta originais dentro de teste
        Arquivo().copiar_imagens(self.get_nome_projeto(), "t", self.get_local())

        # Lista das imagens no diretorio de teste
        self._listaImagensOriginais = Listar().listar_imagens(self._localImagensOriginaisTeste)

        # Armazena o total de imagens contidas no diretorio de teste
        self._numeroTotalImagens = len(self._listaImagensOriginais)

    ##########################################################################################
    # Verificação
    ##########################################################################################
    def _carregar_imagem(self):
        imagem_atual = self.get_lista_imagens_originais()[self.get_numero_imagem_atual() - 1]

        original = os.path.join(self._localImagensOriginaisTeste, imagem_atual.split("\\")[-1])
        img_original = wx.Image(original, wx.BITMAP_TYPE_ANY)
        img_original = img_original.Scale(500, 300)
        self.bitmapOriginal.SetBitmap(wx.Bitmap(img_original))

        self.qtdDetectada = Detectar().detectar_imagem(self.get_nome_projeto(), original, self.get_arquivo_cascade())

        detectada = os.path.join(self._localImagensDetectadasTeste, imagem_atual.split("\\")[-1])
        img_detectada = wx.Image(detectada, wx.BITMAP_TYPE_ANY)
        self.bitmapDetectado.SetBitmap(wx.Bitmap(img_detectada))

        self.panel9.Refresh()

        self.staticTextNomeImagem.SetLabel(imagem_atual.split("\\")[-1])

        altura, largura = Imagem().tamanho_imagem(self.get_lista_imagens_originais()[self.get_numero_imagem_atual() - 1])
        self.staticTextTamanho.SetLabel(str(largura) + "x" + str(altura))

        self.staticTextNObjetosDetectados.SetLabel(str(self.qtdDetectada))

    def _proxima_imagem(self, event):
        """Carrega a próxima imagem na tela e adiciona 1 no número da imagem atual"""

        self._add_info()

        self._adicionar_imagem()
        self._carregar_imagem()

    def _adicionar_imagem(self):
        """Adiciona 1 no número da imagem atual, atualiza o nome do botão, verifica se pode habilitar o botão Resultado e zera os valores"""

        self.set_numero_imagem_atual(self.get_numero_imagem_atual() + 1)
        self.buttonProxima.SetLabel("Imagem " + str(self.get_numero_imagem_atual()) + " de " + str(self.get_numero_total_imagens()) + " >")
        self._habilita_resultado()
        self._zerar_valores()

    def _habilita_resultado(self):
        """Habilita o botão resultado caso tenha acabado as imagens"""

        if self.get_numero_total_imagens() == self.get_numero_imagem_atual():
            self.buttonProxima.Enable(False)
            self.buttonResultado.Enable(True)

    def _zerar_valores(self):
        self.nObj.SetLabel("")
        self.nObj.SetFocus()
        self.fp.SetLabel("")
        self.fn.SetLabel("")
        self.staticTextNomeImagem.SetLabel("")

    def _add_info(self):

        qtd = self.qtdDetectada
        n_obj = self.nObj.GetValue()
        fp = self.fp.GetValue()
        fn = self.fn.GetValue()

        if qtd == "":
            qtd = 0

        if n_obj == "":
            n_obj = 0

        if fp == "":
            fp = 0

        if fn == "":
            fn = 0

        self._info[self.get_numero_imagem_atual() - 1][0] = qtd
        self._info[self.get_numero_imagem_atual() - 1][1] = n_obj
        self._info[self.get_numero_imagem_atual() - 1][2] = fp
        self._info[self.get_numero_imagem_atual() - 1][3] = fn

    ##########################################################################################
    # Cursor
    ##########################################################################################
    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

    ##########################################################################################
    # Fluxo de Tela
    ##########################################################################################
    def _go_resultado(self, event):
        """Fecha a janela atual e abre a janela TestarImagensResultado"""

        self._add_info()
        self.Close(True)
        TestarImagensResultado(self.get_nome_projeto(), self._info, self.get_arquivo_cascade()).Show(True)

    def _sair(self, event):
        """Fecha a janela atual e retorna a janela principal, finalizando toda a operação e apagando as imagens"""

        from data.views.Main import Main
        Main().Show(True)
        self.Close(True)
