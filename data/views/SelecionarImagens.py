#!/usr/bin/env python
# -*- coding: utf-8 -*-  

import wx
from data.views.FiltroImagens import FiltroImagens
from data.views.LocaisIguais import LocaisIguais


class SelecionarImagens(wx.Frame):

    def __init__(self, projeto):

        self._dirPickerPosState = False
        self._dirPickerNegState = False
        self._projeto = projeto
        
        self._status = [self._dirPickerPosState, self._dirPickerNegState]
        
        self._dirPos = ""
        self._dirNeg = ""
        
        ##########################################################################################
        # Cria o Frame
        ##########################################################################################
        
        wx.Frame.__init__(self, None, id=wx.ID_ANY, title="Novo Projeto 2-6: Selecionar Imagens | " + projeto.get_nome_projeto(), pos=wx.DefaultPosition, size=wx.Size(350, 213), style=wx.CAPTION | wx.TAB_TRAVERSAL)
        
        self.SetSizeHints(wx.DefaultSize, wx.DefaultSize)
        
        b_sizer_frame = wx.BoxSizer(wx.VERTICAL)
        
        self.panelMain = wx.Panel(self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.TAB_TRAVERSAL)
        b_sizer486 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer121 = wx.BoxSizer(wx.VERTICAL)
        
        self.staticText23 = wx.StaticText(self.panelMain, wx.ID_ANY, "Selecione o diretório onde as imagens estão localizadas", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText23.Wrap(-1)
        b_sizer121.Add(self.staticText23, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer122 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.staticText231 = wx.StaticText(self.panelMain, wx.ID_ANY, "Estas imagens farão parte do projeto ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText231.Wrap(-1)
        b_sizer122.Add(self.staticText231, 0, wx.ALIGN_CENTER, 0)
        
        self.staticTextNomeProjeto = wx.StaticText(self.panelMain, wx.ID_ANY, "", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticTextNomeProjeto.Wrap(-1)
        self.staticTextNomeProjeto.SetFont(wx.Font(wx.NORMAL_FONT.GetPointSize(), 70, 90, 92, False, wx.EmptyString))
        
        b_sizer122.Add(self.staticTextNomeProjeto, 0, 0, 0)
        
        self.staticText30 = wx.StaticText(self.panelMain, wx.ID_ANY, ".", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText30.Wrap(-1)
        b_sizer122.Add(self.staticText30, 0, 0, 0)
        
        b_sizer121.Add(b_sizer122, 1, wx.ALIGN_CENTER, 5)
        
        b_sizer486.Add(b_sizer121, 0, wx.ALL | wx.EXPAND, 10)
        
        b_sizer55 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer67 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer61 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.staticText2951 = wx.StaticText(self.panelMain, wx.ID_ANY, "Positivas:", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText2951.Wrap(-1)
        b_sizer61.Add(self.staticText2951, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer67.Add(b_sizer61, 1, wx.EXPAND, 0)
        
        b_sizer611 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.staticText29511 = wx.StaticText(self.panelMain, wx.ID_ANY, "Negativas: ", wx.DefaultPosition, wx.DefaultSize, 0)
        self.staticText29511.Wrap(-1)
        b_sizer611.Add(self.staticText29511, 0, wx.ALIGN_CENTER, 0)
        
        b_sizer67.Add(b_sizer611, 1, wx.EXPAND, 5)
        
        b_sizer55.Add(b_sizer67, 0, wx.EXPAND | wx.LEFT, 0)
        
        b_sizer68 = wx.BoxSizer(wx.VERTICAL)
        
        b_sizer56 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.dirPickerPos = wx.DirPickerCtrl(self.panelMain, wx.ID_ANY, wx.EmptyString, "Imagens Positivas", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_SMALL | wx.DIRP_USE_TEXTCTRL)
        b_sizer56.Add(self.dirPickerPos, 1, 0, 0)
        
        b_sizer68.Add(b_sizer56, 1, wx.EXPAND, 0)
        
        b_sizer57 = wx.BoxSizer(wx.HORIZONTAL)
        
        self.dirPickerNeg = wx.DirPickerCtrl(self.panelMain, wx.ID_ANY, wx.EmptyString, "Imagens Negativas", wx.DefaultPosition, wx.DefaultSize, wx.DIRP_CHANGE_DIR | wx.DIRP_SMALL | wx.DIRP_USE_TEXTCTRL)
        b_sizer57.Add(self.dirPickerNeg, 1, 0, 0)
        
        b_sizer68.Add(b_sizer57, 1, wx.EXPAND, 0)
        
        b_sizer55.Add(b_sizer68, 1, wx.EXPAND, 0)
        
        b_sizer486.Add(b_sizer55, 0, wx.ALL | wx.EXPAND, 10)
        
        b_sizer484 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer487 = wx.BoxSizer(wx.HORIZONTAL)
        
        b_sizer43 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonCancelar = wx.Button(self.panelMain, wx.ID_ANY, "Cancelar", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer43.Add(self.buttonCancelar, 0, wx.ALIGN_LEFT, 0)
        
        b_sizer487.Add(b_sizer43, 1, 0, 0)
        
        b_sizer44 = wx.BoxSizer(wx.VERTICAL)
        
        self.buttonAvancar = wx.Button(self.panelMain, wx.ID_ANY, "Avançar >", wx.DefaultPosition, wx.Size(130, 30), 0)
        b_sizer44.Add(self.buttonAvancar, 0, wx.ALIGN_RIGHT, 0)
        
        b_sizer487.Add(b_sizer44, 1, 0, 0)
        
        b_sizer484.Add(b_sizer487, 1, 0, 5)
        
        b_sizer486.Add(b_sizer484, 0, wx.ALL | wx.EXPAND, 10)
        
        self.panelMain.SetSizer(b_sizer486)
        self.panelMain.Layout()
        b_sizer_frame.Add(self.panelMain, 0, wx.EXPAND, 0)
        
        self.Centre(wx.BOTH)

        ##########################################################################################
        # Configura Tela
        ##########################################################################################
        
        self.buttonAvancar.Enable(False)

        ##########################################################################################
        # Globais
        ##########################################################################################
        
        # Nome do projeto na barra de titulos
        self.staticTextNomeProjeto.SetLabel(projeto.get_nome_projeto())
        
        # Chama o metodo _dirPickerPosOnDirChanged para verificar se foi informado algum texto no campo para habilitar o botão avançar
        self.dirPickerPos.Bind(wx.EVT_DIRPICKER_CHANGED, self._dir_picker_pos_on_dir_changed)
        
        # Chama o metodo _dirPickerNegOnDirChanged para verificar se foi informado algum texto no campo para habilitar o botão avançar
        self.dirPickerNeg.Bind(wx.EVT_DIRPICKER_CHANGED, self._dir_picker_neg_on_dir_changed)

        ##########################################################################################
        # Cursor
        ##########################################################################################
        
        self.buttonCancelar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonCancelar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)
        
        self.buttonAvancar.Bind(wx.EVT_ENTER_WINDOW, self._mudar_cursor)
        self.buttonAvancar.Bind(wx.EVT_LEAVE_WINDOW, self._cursor_normal)

        ##########################################################################################
        # Eventos de disparo
        ##########################################################################################
        
        # Chama o metodo _fechar para fechar a view atual
        self.buttonCancelar.Bind(wx.EVT_BUTTON, self._fechar)
        
        # Chama o metodo _goFiltroImagens para fechar esta view e abrir FiltroImagens.py
        self.buttonAvancar.Bind(wx.EVT_BUTTON, self._go_filtro_imagens)

##########################################################################################
# Metodos Segurança
##########################################################################################

    def _dir_picker_pos_on_dir_changed(self, event):
        self._dirPos = self.dirPickerPos.GetPath()
        self._status[0] = True
        
        if self._status[1]:
            self.buttonAvancar.Enable(True)

    def _dir_picker_neg_on_dir_changed(self, event):
        self._dirNeg = self.dirPickerNeg.GetPath()
        self._status[1] = True
        
        if self._status[0]:
            self.buttonAvancar.Enable(True)

##########################################################################################
# Cursor
##########################################################################################

    def _mudar_cursor(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        self.Refresh()

    def _cursor_normal(self, event):
        self.SetCursor(wx.Cursor(wx.CURSOR_DEFAULT))
        self.Refresh()

##########################################################################################
# Fluxo de Tela
##########################################################################################

    def _go_filtro_imagens(self, event):
        if self._dirPos == self._dirNeg:
            LocaisIguais(self._projeto).Show(True)
        else:
            self._projeto.set_imagens_positivas(self._dirPos)
            self._projeto.set_imagens_negativas(self._dirNeg)
            
            FiltroImagens(self._projeto).Show(True)
        
        self.Close(True)

    def _fechar(self, event):
        self.Close(True)
        from data.views.Main import Main
        Main().Show(True)
