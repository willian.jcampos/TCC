# -*- mode: python -*-

datas = [('icons\\*', 'icons'),
         ('sample\\bin\\*', 'sample\\bin')
		]

block_cipher = None

a = Analysis(['SistemaDetectaObjetos.py'],
             pathex=['D:\\TCC'],
             binaries=[],
             datas=datas,
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
			 noarchive=False)

pyz = PYZ(a.pure,
          a.zipped_data,
          cipher=block_cipher)

exe = EXE(pyz,
          a.scripts,
		  exclude_binaries=True,
          name='SistemaDetectaObjetos',
          debug=False,
		  bootloader_ignore_signals=False,
          strip=False,
          upx=False,
          console=False,
          icon='icons\\icon.ico')
		  
coll = COLLECT(exe,
               a.binaries,
			   a.zipfiles,
			   a.datas,
			   name='SistemaDetectaObjetos',
			   strip=False,
			   upx=False)