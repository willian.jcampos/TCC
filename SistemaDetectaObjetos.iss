#define MyAppName "Detector de Objetos"
#define MyAppVersion "1.0.0.0"
#define MyFileVersion "1.0.0.1"
#define MyAppPublisher "Instituto Federal de Goi�s (IFG), Edu"
#define MyAppExeName "SistemaDetectaObjetos.exe"

[Setup]
AppId={{B3463C34-EFB3-497B-ADBE-DC933D0330AB}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppVerName={#MyAppName}
AppPublisher={#MyAppPublisher}
VersionInfoVersion={#MyFileVersion}
DefaultDirName=C:\Detector-de-Objetos
DefaultGroupName={#MyAppName}
DisableProgramGroupPage=yes
DisableDirPage=yes
DisableWelcomePage=no
LicenseFile=D:\TCC\LICENSE.txt
InfoBeforeFile=D:\TCC\INFO.txt
InfoAfterFile=D:\TCC\README.txt
OutputDir=D:\TCC\dist
OutputBaseFilename=[beta]detector-objetos-setup
SetupIconFile=D:\TCC\icons\install.ico
Compression=lzma
SolidCompression=yes
UninstallDisplayIcon=D:\TCC\icons\icon.ico
WizardImageFile=D:\TCC\icons\WizardImage.bmp
WizardSmallImageFile=D:\TCC\icons\WizardSmallImage.bmp


[Languages]
Name: "brazilianportuguese"; MessagesFile: "compiler:Languages\BrazilianPortuguese.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"
Name: "quicklaunchicon"; Description: "{cm:CreateQuickLaunchIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; OnlyBelowVersion: 0,6.1

[Files]
Source: "D:\TCC\dist\SistemaDetectaObjetos\SistemaDetectaObjetos.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "D:\TCC\dist\SistemaDetectaObjetos\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
Source: "D:\TCC\icons\install.ico"; DestDir: "{app}"; Flags: ignoreversion

[Icons]
Name: "{group}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{group}\{cm:UninstallProgram,{#MyAppName}}"; Filename: "{uninstallexe}"; IconFilename: "{app}\icons\install.ico"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

